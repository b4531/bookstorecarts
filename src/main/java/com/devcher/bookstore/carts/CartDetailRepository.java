package com.devcher.bookstore.carts;

import com.devcher.bookstore.carts.models.CartDetail;
import com.devcher.bookstore.carts.models.CartHeader;
import com.devcher.bookstore.carts.models.Role;
import com.devcher.bookstore.carts.models.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class CartDetailRepository {
    private List<CartHeader> cartHeaders = new ArrayList<>(Arrays.asList(
            new CartHeader(0L, 1L),
            new CartHeader(1L, 2L),
            new CartHeader(2L, 3L)
    ));

    private List<Role> roles = new ArrayList<>(Arrays.asList(
            new Role(0L, "ADMIN"),
            new Role(1L, "USER")
    ));

    private List<User> users = new ArrayList<>(Arrays.asList(
            new User(0L, "Ilya", "+79803234567", "500a5@gamil.com", "null", "г. Белгород, ул. Королёва, 2а",roles.get(1)),
            new User(1L, "Artem", "+7980338819", "artem@gmail.com", "null", "г. Белгород, ул. Мичурина, 3",roles.get(0)),
            new User(2L, "Vlad", "+79802222456", "vlad@gmail.com", "null", "г. Белгород, ул. Гагарина, 4",roles.get(1))
    ));

    private List<CartDetail> cartDetails = new ArrayList<>(Arrays.asList(
            new CartDetail(0L, 5, cartHeaders.get(0), 2L),
            new CartDetail(1L, 3, cartHeaders.get(1), 0L),
            new CartDetail(2L, 8, cartHeaders.get(2), 1L)
    ));

    public List<CartDetail> findAll()
    {
        return cartDetails;
    }

    public void save(CartDetail cartDetail)
    {
        cartDetails.add(cartDetail);
    }

    public CartDetail findById(Long id)
    {
        return cartDetails.get(id.intValue());
    }

    public void deleteById(Long id)
    {
        cartDetails.remove(id.intValue());
    }
}
