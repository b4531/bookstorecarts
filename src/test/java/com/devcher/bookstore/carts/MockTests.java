package com.devcher.bookstore.carts;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MockTests
{
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void shouldReturnAllCarts() throws Exception
	{
		mockMvc
				.perform(MockMvcRequestBuilders
						.get("/user/carts")
						.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[*].count").exists());
	}

	@Test
	public void shouldReturnCart() throws Exception
	{
		checkCartById(0L);
	}

	@Test
	public void shouldCreateAndDeleteNewCart() throws Exception
	{
		mockMvc
				.perform(MockMvcRequestBuilders
						.post("/user/carts")
						.contentType(MediaType.APPLICATION_JSON)
						.content("{\"id\":3,\"count\":1,\"cartHeader\":{\"id\":0,\"user\":1},\"book\":0}")
						.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().isOk());

		checkCartById(3L);

		mockMvc
				.perform(MockMvcRequestBuilders
						.delete("/user/carts/3")
						.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().isOk());
	}

	private void checkCartById(Long id) throws Exception
	{
		mockMvc
				.perform(MockMvcRequestBuilders
						.get(String.format("/user/carts/%d", id))
						.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.count").exists());
	}
}
