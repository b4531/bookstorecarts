package com.devcher.bookstore.carts;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CartsApplicationTests
{
	@Autowired
	private CartDetailController cartDetailController;

	@Autowired
	private CartDetailRepository cartDetailRepository;


	@Test
	public void contextLoads()
	{
		assertThat(cartDetailController).isNotNull();
		assertThat(cartDetailRepository).isNotNull();
	}
}
